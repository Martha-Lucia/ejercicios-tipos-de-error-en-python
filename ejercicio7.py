# Martha Cango
# martha.cango@unl.edu.ec

# Solucione los errores
# Ponga el tipo de error(sintaxis/tiempo de ejecución/semántico)
# en la declaración de impresión en la línea 10 (solo será de un tipo)
# El propósito de este código es imprimir "positivo" si x es positivo,
# "negativo" si x es negativo y "cero" si x no es ni positivo ni negativo.

x = 10
if x < 0:
	print("negative")
elif x > 0:
	print("positive")
else:
	print("zero")


print("semantic")